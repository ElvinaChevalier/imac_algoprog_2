#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
   int leftIndex;
   leftIndex=(nodeIndex*2)+1;
   return leftIndex;
}

int Heap::rightChild(int nodeIndex)
{
    int rightIndex;
    rightIndex=(nodeIndex*2)+2;
    return rightIndex;
}

void Heap::insertHeapNode(int heapSize, int value)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i = heapSize;
    this->get(i)=value;
    while(i>0 && this->get(i)>this->get((i-1)/2)){
        swap(i,(i-1)/2);
        i=(i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    int i_max = nodeIndex;
    int Size = heapSize;
    int rightIndex=this->rightChild(nodeIndex);
    int leftIndex=this->leftChild(nodeIndex);
    if(rightIndex<Size && this->get(nodeIndex)<this->get(rightIndex)){
        i_max=this->rightChild(nodeIndex);
    }
    if(leftIndex<Size && this->get(i_max)<this->get(leftIndex)){
        i_max=this->leftChild(nodeIndex);

    }

    if(i_max!=nodeIndex){
        swap(nodeIndex,i_max);
        this->heapify(Size,i_max);
    }
}

void Heap::buildHeap(Array& numbers)
{
    for (uint i=0;i<numbers.size();i++) {
        heapify(i,i);

    }

}

void Heap::heapSort()
{
    for (int i = this->size()-1; i>=0; i--){
            this->swap(0,i);
            this->heapify(i, 0);
        }

}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
    w->show();

    return a.exec();
}
