#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if (origin.size() > 1){
        // initialisation
        Array& first = w->newArray(origin.size()/2);
        Array& second = w->newArray(origin.size()-first.size());

        // split
        for (int i=0; i<origin.size(); i++){
                if (i<first.size()){
                    first[i]=origin[i];
                }else{
                    second[i-first.size()] = origin[i];
                }
        }


        // recursiv splitAndMerge of lowerArray and greaterArray
        splitAndMerge(first);
        splitAndMerge(second);
        // merge
        merge(first, second,origin);
    }
}

void merge(Array& first, Array& second, Array& result)
{
    int i = 0;
    int j = 0;
    int count = 0;
    while( i< first.size()  && j< second.size()){
        if (first[i] < second[j]){
            result[count]=first[i];
            i++;
            count++;
         }else{
            result[count]=second[j];
            j++;
            count++;
         }
    }
    while( i< first.size()){
        result[count]=first[i];
        i++;
        count++;
    }
    while ( j< second.size()){
        result[count]=second[j];
        j++;
        count++;
   }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
