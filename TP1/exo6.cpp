#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    int nbNoeud;
};

struct DynaTableau{
    int* donnees;
    int nbDonnees;
    int capacite;
};


void initialise(Liste* liste)
{
    liste->premier = NULL;
    liste->nbNoeud = 0;
}

bool est_vide(const Liste* liste)
{
    if(liste->nbNoeud == 0){
         return true;
    }else{
         return false;
    }
}

void ajoute(Liste* liste, int valeur)
{
        Noeud * noeud = new Noeud;
        noeud->donnee = valeur;
        noeud->suivant = NULL;

        if(liste->premier == NULL){
            liste->premier = noeud;

        }else{
            Noeud * stock = liste->premier;

            while(stock->suivant != NULL){
                stock = stock->suivant;

            }
            stock->suivant=noeud;
       }

        liste->nbNoeud = liste->nbNoeud + 1;

}


void affiche(const Liste* liste)
{

        Noeud * stock = liste->premier;

        while(stock != NULL){
            cout <<  stock->donnee << endl;
            stock = stock->suivant;

        }
        cout << endl;
}

int recupere(const Liste* liste, int n)
{
    int count = 0;
    Noeud * stock = liste->premier;
    while (n!=count && n<liste->nbNoeud){
        stock=stock->suivant;
        count++;
    }
    return stock->donnee;
}

int cherche(const Liste* liste, int valeur)
{
   int count=0;
   Noeud* stock=liste->premier;
   while(stock->donnee!=valeur && stock!=NULL){
       stock=stock->suivant;
       count++;
   }
   if(stock==NULL){
       return -1;
   }else{
       return count;
   }
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud * stock = liste->premier;

    for(int i=0; i<n-1; i++){
        stock = stock->suivant;
    }
    stock->donnee=valeur;


}

void ajoute(DynaTableau* tableau, int valeur)
{
      tableau->donnees[tableau->nbDonnees]= valeur;
      tableau->nbDonnees++;



}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite = capacite;
    tableau->donnees = (int*) malloc (sizeof(int)*tableau->capacite);
    tableau->nbDonnees = 0;
}

bool est_vide(const DynaTableau* liste)
{
    if(liste->nbDonnees == 0){
            return true;
        }else{
            return false;
        }
}

void affiche(const DynaTableau* tableau)
{
        for(int i=0; i<tableau->nbDonnees; i++){
            cout << tableau->donnees[i] << endl;
        }

}

int recupere(const DynaTableau* tableau, int n)
{
        return tableau->donnees[n];

}

int cherche(const DynaTableau* tableau, int valeur)
{
    int count;
    while(tableau->donnees[count]!=valeur && count<tableau->nbDonnees){
        count++;
    }
    if(tableau->donnees[count]==valeur){
        return count;
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
   tableau->donnees[n] = valeur;

}

//pousse file ajoute à la fin
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}


//pousse pile ajoute au début
void pousse_pile(Liste* liste, int valeur)
{
   Noeud* noeud=new Noeud();
   noeud->suivant=liste->premier;
   noeud->donnee=valeur;
   liste->premier=noeud;



}

//retire file enlève fin
int retire_pile(Liste* liste){
    int valeur=liste->premier->donnee;
    liste->premier=liste->premier->suivant;
    return valeur;
}

//retire file enlève fin
int retire_file(Liste* liste)
{
    Noeud * stock = liste->premier;
    int valeur = stock->donnee;

    liste->premier = liste->premier->suivant;
    liste->nbNoeud = liste->nbNoeud - 1;

        return valeur;

}





int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste  " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste  " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements apres stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }


    int compteur = 10;

    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }
/*
    compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "OnO" << std::endl;
    }
*/
    return 0;
}
