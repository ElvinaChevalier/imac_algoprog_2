#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    Node* left;
    Node* right;
    int Nodevalue;

    void initNode(int value)
    {
        // init initial node without children
        left=nullptr;
        right=nullptr;
        Nodevalue=value;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        if(value<Nodevalue){
            if(left==nullptr){
                left=createNode(value);
            }else{
                left->insertNumber(value);
            }
        }else{
            if(right==nullptr){
                right=createNode(value);
            }else{
                right->insertNumber(value);
            }
        }

    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        uint h1=0;
        uint h2=0;

        if(left!=nullptr){
            h1= 1+ left->height();
        }
        if(right!=nullptr){
            h2= 1+ right->height();
        }
        if(h1<h2){
            return h2;
        }else {
            return h1;
        }

            return 1;
        }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        uint nodes = 1;
        if (left != nullptr){
            nodes += left->nodesCount();
        }
        if (right != nullptr){
            nodes += right->nodesCount();
        }
            return nodes;

	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if(left==nullptr && right==nullptr){
            return true;
        }
        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if(isLeaf()){
            leaves[leavesCount]=this;
            leavesCount++;
        }else{
            if(left!=nullptr){
                left->allLeaves(leaves,leavesCount);
            }
            if(right!=nullptr){
                right->allLeaves(leaves,leavesCount);
            }
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if(left!=nullptr){
            left->inorderTravel(nodes,nodesCount);
        }

        nodes[nodesCount]=this;
        nodesCount++;

        if(right!=nullptr){
            right->inorderTravel(nodes,nodesCount);
        }


	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount]=this;
        nodesCount++;

        if(left!=nullptr){
            left->preorderTravel(nodes,nodesCount);
        }

        if(right!=nullptr){
            right->preorderTravel(nodes,nodesCount);
        }

	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if(left!=nullptr){
            left->postorderTravel(nodes,nodesCount);
        }

        if(right!=nullptr){
            right->postorderTravel(nodes,nodesCount);
        }

        nodes[nodesCount]=this;
        nodesCount++;
	}

	Node* find(int value) {
        // find the node containing value
        if(Nodevalue==value){
            return this;
        }else if(Nodevalue>value && left!=nullptr){
            return left->find(value);
        }else if(Nodevalue<value && right!=nullptr){
            return right->find(value);
        }
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return Nodevalue;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}
